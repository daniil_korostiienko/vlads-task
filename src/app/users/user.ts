export class User {
  _id: string;
  _v: string;
  name: string;
  email: string;
  number: number;
}
