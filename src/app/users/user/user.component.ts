import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UsersService } from '../users.service';
import { User } from '../user';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  providers: [
    UsersService
  ]
})
export class UserComponent implements OnInit, OnDestroy {
  user: User;
  sub: any;

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this.usersService.getUser(id).subscribe(
        user => this.user = user);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  goBack() {
    let link = ['/users'];
    this.router.navigate(link);
  }

  saveUser(user: User) {
    this.usersService.saveUser(user).subscribe(this.goBack());
  }

  deleteUser(id: string) {
    this.usersService.deleteUser(id).subscribe(this.goBack());
  }
}
