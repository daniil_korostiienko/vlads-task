import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { User }     from './user';

@Injectable()
export class UsersService {
  constructor(private http: Http) {

  }

  private host = 'http://localhost:3001/';
  private path = this.host + 'users/';

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  getUsers (): Observable<User[]> {
    return this.http
      .get(this.path)
      .map(this.extractData);
  }

  getUser(id: string): Observable<User> {
    return this.http
      .get(this.path + id)
      .map(this.extractData);
  }

  createUser(user: User): Observable<Response> {
    return this.http
      .post(this.path + 'newUserId', user);
  }

  saveUser(user: User): Observable<Response> {
    return this.http
      .put(this.path + user._id, user);
  }

  deleteUser(id: string): Observable<Response> {
    return this.http
      .delete(this.path + id);
  }

}
