import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm }    from '@angular/common';
import { UsersService } from "./users.service";
import { User } from "./user";

@Component({
  selector: 'users',
  providers: [
    UsersService
  ],
  directives: [],
  pipes: [ ],
  templateUrl: './users.template.html'
})
export class Users implements OnInit {
  users: User[] = [];
  formActive = true;
  newUser: User = {
    name: '',
    _id: '',
    _v: '',
    email: '',
    number: 0
  };

  constructor(private usersService: UsersService, private router: Router) {

  }

  ngOnInit() {
    this.getUsers()
  }

  getUsers() {
    this.usersService.getUsers().subscribe(
      users => this.users = users
    );
  }

  createUser(user: User) {
    this.usersService.createUser(user).subscribe(() => {
      this.formActive = false;
      this.newUser = {
        name: '',
        _id: '',
        _v: '',
        email: '',
        number: 0
      };
      setTimeout(() => this.formActive = true, 0);  //form reset workaround
      this.getUsers();
    })
  }

  goToUser(user: User) {
    let link = ['/users', user._id];
    this.router.navigate(link);
  }

}
